function script()
{
    var M;
    var camera;
    this.cube;
    var light;
    var videoWebRTC;
    var videoTexture;
    var ws;
    var video;
    var webRtcPeer;
    var lasttime = getCurrentTime();

    this.preLoad = function(loader)
    {
    videoWebRTC = loader.loadVideo( {url:"data/VilleVertebrale.m4v"} );
    }

    // TO DO
    //this.close = function() {
    //    dispose()
    //    ws.close();
    //}

    this.setup = function()
    {
        M = this.getContext();
        R = new Mobilizing.Renderer3D();
        M.addComponent(R);

        camera = new Mobilizing.Camera();
        //configure the camera to be ajusted to the screen, origin at top-left
        camera.setToPixel();
        R.addCamera(camera);

        var canvasSize = R.getCanvasSize();

        light = new Mobilizing.Light();
        light.setDistance(5000);
        light.transform.setLocalPosition(canvasSize.width/2 + 500,-canvasSize.height/2 + 500,500);
        R.addToCurrentScene(light);

        light2 = new Mobilizing.Light();
        light2.setDistance(5000);
        light2.transform.setLocalPosition(canvasSize.width/2 - 500,-canvasSize.height/2 - 500,-500);
        R.addToCurrentScene(light2);

        var video_ = videoWebRTC.getValue();
        videoTexture = new Mobilizing.VideoTexture( {video:video_, autoPlay:false} );

        this.cube = new Mobilizing.Mesh({primitive:"box",
                                         width:videoTexture.getWidth()/2,
                                         height:videoTexture.getHeight()/2,
                                         depth:videoTexture.getHeight()/2});


        this.cube.transform.setLocalPosition(canvasSize.width/2,-canvasSize.height/2,0);

        this.cube.material.setTexture(videoTexture);
        R.addToCurrentScene(this.cube);

        // kurento viewer
        video = videoTexture._video; //get video element from VideoTexture
        newWebsocket();
    };
    
    var speed = .1;

    this.update = function()
    {
        var rot = this.cube.transform.getLocalRotation();
        this.cube.transform.setLocalRotation(rot.x+speed, rot.y+speed, 0);

        videoTexture.update();

        updateViewer();
    };

    // kurento viewer function compatible kurento-one2many-call-multi-admin
    function newWebsocket() {
        ws = new WebSocket('ws://192.168.1.12:8443/one2many');
        lasttime = getCurrentTime();

        ws.onmessage = function(message) {
            var parsedMessage = JSON.parse(message.data);
            console.info('Received message: ' + message.data);
        
            switch (parsedMessage.id) {
            case 'viewerResponse':
                viewerResponse(parsedMessage);
                break;
            case 'stopCommunication':
                dispose();
                break;
            case 'iceCandidate':
                webRtcPeer.addIceCandidate(parsedMessage.candidate)
                break;
            default:
                console.error('Unrecognized message', parsedMessage);
            }
        }

        ws.onopen = function(e) {
            console.log('Websocket onopen !');
            viewer();
        };
        
        ws.onclose = function(e) {
            console.log('Websocket onclose !');
            dispose();
        };
        ws.onerror = function (e) {
            console.log('Websocket onError !');
            dispose();
        };
    }

    function updateViewer()
    {
        // if not webRtcPeer call viewer every second
        var elapsedtime = getCurrentTime() - lasttime;
        if( elapsedtime > 1000) {
            lasttime = getCurrentTime();
            if(ws.readyState === ws.CLOSED){
                console.log('ws state CLOSED' );
                dispose();
                newWebsocket();
            }
            else if(ws.readyState === ws.CLOSING){
                console.log('ws state CLOSING' );
                dispose();
                newWebsocket();
            }
            else if(ws.readyState === ws.OPEN){
                console.log('ws state OPEN' );
            }
            else if(ws.readyState === ws.CONNECTING){
                console.log('ws state CONNECTING' );
            }
            if (!webRtcPeer) {
                console.log('Call viewer');
                viewer();
            }
        }
    }

    function getCurrentTime() {
        var my_current_timestamp;
        my_current_timestamp = new Date();      //stamp current date & time
        return my_current_timestamp.getTime();
    }

    function viewerResponse(message) {
        if (message.response != 'accepted') {
            var errorMsg = message.message ? message.message : 'Unknow error';
            console.log('Call not accepted for the following reason: ' + errorMsg);
            dispose();
        } else {
            webRtcPeer.processAnswer(message.sdpAnswer);
        }
    }
    
    function viewer() {
        if (!webRtcPeer && ws.readyState === ws.OPEN) {
            showSpinner(video);
    
            var options = {
                remoteVideo: video,
                onicecandidate : onIceCandidate
            }
    
            webRtcPeer = kurentoUtils.WebRtcPeer.WebRtcPeerRecvonly(options, function(error) {
                if(error) return onError(error);
    
                this.generateOffer(onOfferViewer);
            });
        }
    }
    
    function onOfferViewer(error, offerSdp) {
        if (error) return onError(error)
    
        var message = {
            id : 'viewer',
            sdpOffer : offerSdp
        }
        sendMessage(message);
    }
    
    function onIceCandidate(candidate) {
        console.log('Local candidate' + JSON.stringify(candidate));
    
        var message = {
            id : 'onIceCandidate',
            candidate : candidate
        }
        sendMessage(message);
    }
    
    function stop() {
        if (webRtcPeer) {
            var message = {
                    id : 'stop'
            }
            sendMessage(message);
            dispose();
        }
    }
    
    function dispose() {
        if (webRtcPeer) {
            webRtcPeer.dispose();
            webRtcPeer = null;
        }
        hideSpinner(video);
    }
    
    function sendMessage(message) {
        var jsonMessage = JSON.stringify(message);
        console.log('Senging message: ' + jsonMessage);

        // 4 values : CONNECTING OPEN CLOSING or CLOSED
        if(ws.readyState === ws.OPEN){
            ws.send(jsonMessage);
        }
    }
    
    function showSpinner() {

    }
    
    function hideSpinner() {

    }
    
    function onError(error) {
        console.error(error);
    }
};
